FROM python:3
RUN groupadd -r flask && useradd --no-log-init -r -g flask flask
COPY . /flask_calculator_app-master1
WORKDIR /flask_calculator_app-master1
RUN python3 -m pip install -r requirements.txt

EXPOSE 5000

CMD ["python3" , "-m" , "flask" , "run" , "--host=0.0.0.0"]
